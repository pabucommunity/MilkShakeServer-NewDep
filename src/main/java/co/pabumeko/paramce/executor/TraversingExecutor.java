package co.pabumeko.paramce.executor;

import java.util.Spliterator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.RecursiveAction;
import java.util.function.Consumer;

public class TraversingExecutor implements Executor {
    private final AsyncCatcherReachedExecutor taskingExecutor;

    public TraversingExecutor(int forkingT,int taskingT,String bound){
        this.taskingExecutor = new AsyncCatcherReachedExecutor(bound,taskingT);
    }

    private <E> void traverseNonFull(Iterable<E> itreator,Consumer<E> action) {
       itreator.forEach(result->{
           taskingExecutor.execute(()->action.accept(result));
       });
    }

    private <E> void traverseNonFull(Iterable<E> itreator,Consumer<E> action, int hold) {
        itreator.forEach(result->{
            taskingExecutor.execute(()->action.accept(result));
        });
    }

    public <E> void traverseParallel(Iterable<E> iterable,Consumer<E> action){
        try{
            if (iterable.spliterator().getExactSizeIfKnown() <= 0){return;}
            CountDownLatch latch = new CountDownLatch((int) iterable.spliterator().getExactSizeIfKnown());
            Consumer<E> fullAction = result -> {
                try{
                    action.accept(result);
                }finally {
                    latch.countDown();
                }
            };
            this.traverseNonFull(iterable,fullAction,10);
            latch.await();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public <E> void traverseLittleParallel(Iterable<E> iterable,Consumer<E> action){
        try{
            if (iterable.spliterator().getExactSizeIfKnown() <= 0){return;}
            CountDownLatch latch = new CountDownLatch((int) iterable.spliterator().getExactSizeIfKnown());
            Consumer<E> fullAction = result -> {
                try{
                    action.accept(result);
                }finally {
                    latch.countDown();
                }
            };
            this.traverseNonFull(iterable,fullAction);
            latch.await();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void execute(Runnable command) {
        this.taskingExecutor.execute(command);
    }

    public class ConcurrentlyTraverse<E> extends RecursiveAction {
        private final Spliterator<E> spliterator;
        private final Consumer<E> action;
        private final long threshold;

        public ConcurrentlyTraverse(Iterable<E> iterable,int threads,Consumer<E> action){
            this.spliterator = iterable.spliterator();
            this.action = action;
            this.threshold = (int)iterable.spliterator().getExactSizeIfKnown() / threads;
        }

        private ConcurrentlyTraverse(Spliterator<E> spliterator,Consumer<E> action,long t) {
            this.spliterator = spliterator;
            this.action = action;
            this.threshold = t;
        }

        @Override
        protected void compute() {
            if (this.spliterator.getExactSizeIfKnown() <= this.threshold) {
                this.spliterator.forEachRemaining(o->{
                    try {
                        this.action.accept(o);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                });
            } else {
                new ConcurrentlyTraverse<>(this.spliterator.trySplit(), this.action, this.threshold).fork();
                new ConcurrentlyTraverse<>(this.spliterator, this.action, this.threshold).fork();
            }
        }
    }

}
