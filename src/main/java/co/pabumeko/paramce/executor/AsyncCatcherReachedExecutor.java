package co.pabumeko.paramce.executor;

import co.pabumeko.paramce.AsyncCatcher;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class AsyncCatcherReachedExecutor implements ExecutorService {
    private final ClassLoader LOADER = this.getClass().getClassLoader();
    private final AtomicInteger threadId = new AtomicInteger();
    private final String bound;
    private final ForkJoinPool executor;

    public AsyncCatcherReachedExecutor(){
        this.bound = "Default";
        this.executor = new ForkJoinPool(Runtime.getRuntime().availableProcessors(),genThreadFactory(this.bound),null,true);
    }

    public AsyncCatcherReachedExecutor(int threads){
        this.bound = "Default";
        this.executor = new ForkJoinPool(threads,genThreadFactory(this.bound),null,true);
    }

    public AsyncCatcherReachedExecutor(String bound){
        this.bound = bound;
        this.executor = new ForkJoinPool(Runtime.getRuntime().availableProcessors(),genThreadFactory(this.bound),null,true);
    }

    public AsyncCatcherReachedExecutor(String bound,int threads){
        this.bound = bound;
        this.executor = new ForkJoinPool(threads,genThreadFactory(this.bound),null,true);
    }

    private ForkJoinPool.ForkJoinWorkerThreadFactory genThreadFactory(String bound){
        return pool -> {
            ForkJoinWorkerThread workerThread = ForkJoinPool.defaultForkJoinWorkerThreadFactory.newThread(pool);
            workerThread.setName("Worker-"+bound+"-"+this.threadId.getAndIncrement());
            workerThread.setDaemon(true);
            workerThread.setPriority(8);
            workerThread.setContextClassLoader(LOADER);
            AsyncCatcher.threadCache.regThread(workerThread);
            return workerThread;
        };
    }

    @Override
    public void shutdown() {
        this.executor.shutdown();
    }

    @Override
    public List<Runnable> shutdownNow() {
        return this.executor.shutdownNow();
    }

    @Override
    public boolean isShutdown() {
        return this.executor.isShutdown();
    }

    @Override
    public boolean isTerminated() {
        return this.executor.isTerminated();
    }

    @Override
    public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
        return this.executor.awaitTermination(timeout,unit);
    }

    @Override
    public <T> Future<T> submit(Callable<T> task) {
        return this.executor.submit(task);
    }

    @Override
    public <T> Future<T> submit(Runnable task, T result) {
        return this.executor.submit(task,result);
    }

    @Override
    public Future<?> submit(Runnable task) {
        return this.executor.submit(task);
    }

    @Override
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) throws InterruptedException {
        return this.executor.invokeAll(tasks);
    }

    @Override
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit) throws InterruptedException {
        return this.executor.invokeAll(tasks,timeout,unit);
    }

    @Override
    public <T> T invokeAny(Collection<? extends Callable<T>> tasks) throws InterruptedException, ExecutionException {
        return this.executor.invokeAny(tasks);
    }

    @Override
    public <T> T invokeAny(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        return this.executor.invokeAny(tasks,timeout,unit);
    }

    @Override
    public void execute(Runnable command) {
        this.executor.execute(command);
    }

    public int getThreads(){
        return this.executor.getParallelism();
    }

    public ForkJoinPool getPool(){
        return this.executor;
    }
}
