package co.pabumeko.paramce;

import net.minecraft.server.MinecraftServer;
import org.bukkit.Bukkit;

public class AsyncCatcher {
    public static final ThreadCache threadCache = new ThreadCache();

    public static void doCatchForce(String reason){
        if (!threadCache.isInGroup() && Thread.currentThread()!= MinecraftServer.getServerInst().primaryThread && !Bukkit.isPrimaryThread()){
           throw new IllegalStateException("Try to "+reason+" off main thread!Returning to main thread...");
        }
    }

    public static boolean checkMain(){
        return threadCache.isInGroup() || Thread.currentThread().getName().contains("Server thread") || Bukkit.isPrimaryThread();
    }
}
