package co.pabumeko.paramce.collections;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Consumer;

public class ListSet<E> extends AbstractSet<E> implements NavigableSet<E> {
    private final List<E> baking = new InternalList();

    @Override
    public E lower(E e) {
        return null;
    }

    @Override
    public E floor(E e) {
        return null;
    }

    @Override
    public E ceiling(E e) {
        return null;
    }

    @Override
    public E higher(E e) {
        return null;
    }

    @Override
    public E pollFirst() {
        return this.baking.remove(0);
    }

    @Override
    public E pollLast() {
        return this.baking.remove(this.baking.size()-1);
    }

    @Override
    public Iterator<E> iterator() {
        return this.baking.iterator();
    }

    @Override
    public NavigableSet<E> descendingSet() {
        return null;
    }

    @Override
    public Iterator<E> descendingIterator() {
        return null;
    }

    @Override
    public NavigableSet<E> subSet(E fromElement, boolean fromInclusive, E toElement, boolean toInclusive) {
        return null;
    }

    @Override
    public NavigableSet<E> headSet(E toElement, boolean inclusive) {
        return null;
    }

    @Override
    public NavigableSet<E> tailSet(E fromElement, boolean inclusive) {
        return null;
    }

    @Override
    public Comparator<? super E> comparator() {
        return null;
    }

    @Override
    public SortedSet<E> subSet(E fromElement, E toElement) {
        return null;
    }

    @Override
    public SortedSet<E> headSet(E toElement) {
        return null;
    }

    @Override
    public SortedSet<E> tailSet(E fromElement) {
        return null;
    }

    @Override
    public E first() {
        return baking.get(0);
    }

    @Override
    public E last() {
        return baking.get(this.baking.size()-1);
    }

    @Override
    public int size() {
        return this.baking.size();
    }

    @Override
    public boolean add(E e){
        return this.baking.add(e);
    }

    @Override
    public boolean remove(Object o) {
        return this.baking.remove(o);
    }

    public boolean isEmpty(){
        return this.baking.isEmpty();
    }

    private final class InternalList extends CopyOnWriteArrayList{
        private final InternalList instance = this;
        private final class COWIterator<E> implements ListIterator<E> {
            /** Snapshot of the array */
            private final Object[] snapshot;
            /** Index of element to be returned by subsequent call to next.  */
            private int cursor;

            private COWIterator(Object[] elements, int initialCursor) {
                cursor = initialCursor;
                snapshot = elements;
            }

            public boolean hasNext() {
                return cursor < snapshot.length;
            }

            public boolean hasPrevious() {
                return cursor > 0;
            }

            @SuppressWarnings("unchecked")
            public E next() {
                if (! hasNext())
                    throw new NoSuchElementException();
                return (E) snapshot[cursor++];
            }

            @SuppressWarnings("unchecked")
            public E previous() {
                if (! hasPrevious())
                    throw new NoSuchElementException();
                return (E) snapshot[--cursor];
            }

            public int nextIndex() {
                return cursor;
            }

            public int previousIndex() {
                return cursor-1;
            }

            public void remove() {
                instance.remove(cursor--);
            }

            public void set(E e) {
                throw new UnsupportedOperationException();
            }

            public void add(E e) {
                throw new UnsupportedOperationException();
            }

            @Override
            public void forEachRemaining(Consumer<? super E> action) {
                Objects.requireNonNull(action);
                Object[] elements = snapshot;
                final int size = elements.length;
                for (int i = cursor; i < size; i++) {
                    @SuppressWarnings("unchecked") E e = (E) elements[i];
                    action.accept(e);
                }
                cursor = size;
            }
        }
        @Override
        public Iterator<E> iterator(){
            return new COWIterator<>(this.toArray(),0);
        }
    }
}
