package co.pabumeko.paramce;

import com.google.common.collect.Sets;

import java.util.Set;

public class ThreadCache {
    private final Set<Thread> threads = Sets.newConcurrentHashSet();

    public void regThread(){
        this.threads.add(Thread.currentThread());
    }

    public void regThread(Thread th){
        this.threads.add(th);
    }

    public void deRegThread(){
        this.threads.remove(Thread.currentThread());
    }

    public boolean isInGroup(){
        return this.threads.contains(Thread.currentThread());
    }

    public boolean isInGroup(Thread thread){
        return this.threads.contains(thread);
    }
}
